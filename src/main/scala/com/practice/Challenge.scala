package com.practice

object Challenge extends App {
  import org.apache.spark.sql.SparkSession

  val spark = SparkSession
    .builder()
    .master("local[*]")
    .appName("Paytm")
    .getOrCreate()

  val df = spark.read
    .option("header", "false")
    .option("delimiter", " ")
    .csv("src/main/resources/2015_07_22_mktplace_shop_web_log_sample.log")

  import org.apache.spark.sql.functions._

  //Q1
  df.groupBy("_c2").agg(count("_c11")).show(100, false)

  //Q2
  //NOT sure how to determine a session

  //Q3
  df.groupBy("_c2").agg(countDistinct("_c11")).show(100, false)

  spark.stop()

}
